let json = {
    "nombre" : "",
    "apellido_paterno" : "",
    "apellido_materno" : "",
    "email" : "",
    "razon" : "",
    "consulta" : ""
}
function rescataForularioParaTransformarJson(){
    let pre = document.getElementById("json");
    pre.classList.remove("hide");
    let correo =  validaEmail();
    if(correo != "Fallo"){
        json.email = document.getElementById("email").value;
    }
    json.nombre = document.getElementById("nombre").value;
    json.apellido_paterno = document.getElementById("apePaterno").value;
    json.apellido_materno = document.getElementById("apeMaterno").value;
    json.razon = document.querySelector("[name=razon]:checked").value;
    json.consulta = document.getElementById("consulta").value;
    pre.innerHTML = JSON.stringify(json);
}
function validaEmail(){
    let contenido = document.getElementById("email").value;
    let conte = contenido.split("@");
    if(!conte[1] || conte[0]==""){
      alert('Email inválido, ingrese datos antes y después de un @');
        correo = "";
        document.getElementById("email").focus();
        return "Fallo";
    }else{
        let cont2=conte[1].split(".");
        if(!cont2[1]){
            alert('Email inválido, ingrese dominio superior');
            correo = "";
            document.getElementById("email").focus();
            return "Fallo";
        }
    }
}
function noNulos(){
    if(document.getElementById("nombre").value  === ""){
        alert("El nombre no puede ser vacío");
        document.getElementById("nombre").focus();
        return null;
    }else if(document.getElementById("apePaterno").value === ""){
        alert("El apellido paterno no puede ser vacío");
        document.getElementById("apePaterno").focus();
        return null;
    }else if(document.getElementById("email").value === ""){
        alert("El email no puede ser vacío");
        document.getElementById("email").focus();
        return null;
    }else if(document.getElementById("consulta").value === ""){
        alert("La consulta no puede ser vacía");
        document.getElementById("consulta").focus();
        return null;
    }else{
      return rescataForularioParaTransformarJson();
    }
}
function soloLetras(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
    especiales = "8-37-39-46";
    tecla_especial = false
    for(var i in especiales){
        if(key == especiales[i]){
            tecla_especial = true;
            break;
        }
    }
    if(letras.indexOf(tecla)==-1 && !tecla_especial){
        return false;
    }
 }